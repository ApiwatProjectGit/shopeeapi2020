﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Repositories
{
    public interface IBaseRepository
    {
        IEnumerable<T> QueryStoreProcedure<T>(string spname, DynamicParameters parameters);
    }
}
