﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        private readonly string connectionString;
        public BaseRepository(string connectionString)
        {
            this.connectionString = connectionString;

        }
        private T WithConnection<T>(Func<IDbConnection, T> getdata)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    return getdata(connection);
                }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        public  IEnumerable<T> QueryStoreProcedure<T>(string spname, DynamicParameters parameters)
        {
            var result = WithConnection(c => c.Query<T>(spname, parameters, commandType: CommandType.StoredProcedure));
            return result;
        }
    }
}
