﻿using ShopeeProject.Web.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Services
{
    public interface IAuthService
    {
        Task<AuthResponseModel> LoginAsync(string username, string password);
    }
}
