﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Newtonsoft.Json;
using ShopeeProject.Web.API.Models;
using ShopeeProject.Web.API.Repositories;

namespace ShopeeProject.Web.API.Services
{
    public class AuthService : IAuthService
    {
        private readonly IBaseRepository baseRepository;
        public AuthService(IBaseRepository baseRepository)
        {
            this.baseRepository = baseRepository;
        }
        public async Task<AuthResponseModel> LoginAsync(string username, string password)
        {
            var spParam = new DynamicParameters();
            var hashPassword = MD5(password);
            spParam.Add("@username", username);
            spParam.Add("@password", hashPassword);

            var result = baseRepository.QueryStoreProcedure<UserModel>("sp_Login", spParam);

            if (result == null)
            {
                return null;
            }
            else
            {
                string authServer = "https://x8ona2vu4c.execute-api.ap-southeast-1.amazonaws.com/Prod/api/auth";
                var param = new
                {
                    username = username,
                    userPassword = password,
                };
                //make param to jsonFormat
                var serializeobj = JsonConvert.SerializeObject(param);

                //create client
                HttpClient httpClient = new HttpClient();
                //pack content
                StringContent stringContent = new StringContent(serializeobj, Encoding.UTF8, "application/json");
                //sent pack
                HttpResponseMessage ResponseMessage = await httpClient.PostAsync(authServer, stringContent);
                //read httpmessage
                var responseContent = await ResponseMessage.Content.ReadAsStringAsync();

                //turn json to AuthResponseModel
                return JsonConvert.DeserializeObject<AuthResponseModel>(responseContent);
            }
        }
        private string MD5(string s)
        {
            using (var provider = System.Security.Cryptography.MD5.Create())
            {
                StringBuilder builder = new StringBuilder();
                foreach (byte b in provider.ComputeHash(Encoding.UTF8.GetBytes(s)))
                    builder.Append(b.ToString("x2").ToLower());

                return builder.ToString();
            }
        }
    }
}
