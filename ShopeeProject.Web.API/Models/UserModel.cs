﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Models
{
    public class UserModel
    {
        public string Firstname { get; set; }
        public string LastName { get; set; }
    }
}
