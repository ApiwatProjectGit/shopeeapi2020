﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Models
{
    public class LoginModel
    {
        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        public string Username { get; set; }
        [Required]
        [MinLength(6)]
        [MaxLength(20)]
        public string Password { get; set; }
    }
}
