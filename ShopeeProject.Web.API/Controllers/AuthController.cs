﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ShopeeProject.Web.API.Models;
using ShopeeProject.Web.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShopeeProject.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;
        public AuthController(IAuthService authService)
        {
            this.authService = authService;
        }
        //POST : api/auth/login
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync([FromBody]LoginModel loginModel)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result =await authService.LoginAsync(loginModel.Username, loginModel.Password);
            if (result != null)
            {
                Response.Cookies.Append("AccessToken", result.AccessToken);
                Response.Cookies.Append("ExpiresIn", result.ExpiresIn.ToString());
                Response.Cookies.Append("RefreshToken", result.RefreshToken);
                return Ok(result);
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize("AppAuthorize")]    
        [HttpGet]
        public IActionResult AuthorizeTest()
        {
            return Ok();
        }
    }
}
